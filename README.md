# Open Payments Conformance Suite

This testing suite is an implementation of [OpenID Conformance Suite](https://gitlab.com/openid/conformance-suite) specifically for Open Payments.

## What is Conformance Suite?

You can get familiar with the architecture by reading the design documents in the [wiki](https://gitlab.com/openid/conformance-suite/-/wikis/Design/structure).

## Build & Run
The application is built on Java Spring on the backend and pure Javascript on the frontend
that uses the public API exposed by the backend.

### Prerequisites
- [Maven](https://maven.apache.org/install.html)
- [Docker](https://docs.docker.com/get-docker/)
- Java 11.0 (any 11.0 version is fine)

### Local Development
Ensure running the command `java -version` shows version 11.0 before executing the following commands.
```
# clone the repo
git clone https://gitlab.com/omertoast/open-payments-conformance-suite
cd open-payments-conformance-suite

# build the backend
mvn clean package

# start docker containers
docker-compose -f docker-compose-dev.yml up
```
**Note:** You can use `docker-compose-dev-mac.yml` if you are on a Mac.

After having prequisites ready and executed commands above, visit https://localhost:8443/ to view the application.
(this may result in a certificate error depending on your browser; on a Mac chrome is perhaps
more likely to let you continue anyway - see this [stack overflow](https://stackoverflow.com/a/31900210/292166) answers for some hints on getting it to work)

The java debugger will be available on http://localhost:9999/.

### Running the backend on the host with Intellij
For faster builds and debugging, you can run the backend on the host machine instead of in a docker container.
To do this, you need to have `docker-compose-dev-no-docker.yml` (or `docker-compose-dev-no-docker-mac.yml` running (a MongoDB instance and a httpd server) depending on your platform.

``$ docker-compose -f docker-compose-dev-nodocker.yml up``

After having docker containers running, use `Conformance` run configuration on Intellij Idea to run the backend on the host machine.

**You can check [Conformance Suite Development Tips](https://gitlab.com/openid/conformance-suite/-/wikis/Developers/Build-&-Run#development-tips) for more detailed explanation of
how to get the suite running.**

## Contributing
You can find `open-payments` package under `src/main/java/net/openid/conformance/` directory. This is where you can add new [test modules](https://gitlab.com/openid/conformance-suite/-/wikis/Design/TestModule)
and [conditions](https://gitlab.com/openid/conformance-suite/-/wikis/Design/Condition). At the top of the directory, you can find the **Open Payments Test Plan** and
**test modules** that are defined in the test plan. Under `src/main/java/net/openid/conformance/condition/` directory, you can find the conditions that are used in the test modules.
You can create new test modules and conditions under these directories. If you want to create a new test module,
you should add it to the test plan in order to use it.
