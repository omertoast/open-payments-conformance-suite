# Rafiki Example Configuration

You can use this configuration as a hello world example. You need to have a local [Rafiki](https://github.com/interledger/rafiki) development environment
in order to run this configuration. You can follow the instructions in the [Rafiki README](https://github.com/interledger/rafiki#local-development) to
setup a local environment.

[Configurations](https://gitlab.com/openid/conformance-suite/-/wikis/Design/Configuration) allows you to test your implementation against a set of [test modules](https://gitlab.com/openid/conformance-suite/-/wikis/Design/TestModule) (scenarios) that are defined in the test plan.
You can replicate the test results over time by using the same configuration.

This configuration uses [seeded](https://github.com/interledger/rafiki/blob/eb03bd440e5faf7086b7fcf39e4d5116fa1ae613/infrastructure/local/seed.primary.yml#L14) payment pointers in the Rafiki environment.

After you have a local Rafiki environment and conformance suite running, visit https://localhost:8443/ to view the application.
Select `JSON` as the configuration format and paste the following configuration in the text area:
```
{
    "alias": "openpayments-test",
    "description": "open payments test plan",
    "paymentPointer": {
        "paymentPointerUrl": "http://localhost:3000/accounts/gfranklin"
    },
    "paymentPointerPeer": {
        "paymentPointerUrl": "http://localhost:4000/accounts/planex"
    }
}
```
Let's break down the configuration:
- `alias`: Used to make any URLs used in the test unique to the user (for example, it affects the redirect url), set to something unique to avoid clashes with other testers, e.g. set to your company name.
This can be left blank and a random one will be generated.
- `description`: Freeform text to help you identify the test run (optional).
- `paymentPointer`: The payment pointer that will be used to test. Values used in this field are [seeded](https://github.com/interledger/rafiki/blob/eb03bd440e5faf7086b7fcf39e4d5116fa1ae613/infrastructure/local/seed.primary.yml#L14) in the local Rafiki dev environment.
- `paymentPointerPeer`: Same as `paymentPointer`. This is the payment pointer that will be used in the tests that require a peer payment pointer.

Congratulations! You just created your first test plan. Now you can start running the **test modules** that are defined in the test plan.
